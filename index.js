

let trainer = {
	name: "Ash Ketchum",
	age: "10",
	pokemon: ["Pikachu", "Charizard", "Squirtle", "Bulbasaur"],
	friends: {
		hoenn: ["May", "Max"],
		kanto: ["Brock", "Misty"],

	}
}

console.log(trainer); //print out trainer object

trainer.talk = "Pikachu I choose you!" //5. Initialize/add the trainer object method named talk that prints out the message Pikachu! I choose you!

// Access the trainer object properties using dot and square bracket notation.
console.log(trainer.name);
console.log("Result of square bracket notation: ")
console.log(trainer["pokemon"]);

//7. Invoke/call the trainer talk object method.
console.log(trainer.talk); 



//Create a constructor for creating a pokemon with the following properties:

function createPokemon (name, level){ 
	this.name = name; 
	this.level = level;
	this.health = 2 * level;
	this.attack = level;
	
	// Create a tackle method that will subtract the health property of the target pokemon object with the attack property of the object that used the tackle method.
	this.tackle = function(target){
		console.log(this.name + " tackled " + target.name)
		let newHealth = target.health - this.attack;

		if(newHealth > 1){
		console.log(target.name + " health is now reduced to " + newHealth);

		// new target object properties
		target.health = newHealth;
		console.log(target);
		}


		// Create a condition in the tackle method that if the health property of the target pokemon object is less than or equal to 0 will invoke the faint method.
		else if(newHealth <= 0){
			console.log(target.name + " health is now reduced to " + newHealth);
			target.faint();

		// new target object properties
			target.health = newHealth;
			console.log(target);	
		}


	}
	// Create a faint method that will print out a message of targetPokemon has fainted.
	// the fnc that will run if the target newHealth <= 0
	this.faint = function(){
		console.log(this.name + " fainted")
	}

}


// Create/instantiate several pokemon object from the constructor with varying name and level properties.
let pikachu = new createPokemon("Pikachu", 12);
console.log(pikachu);

let charizard = new createPokemon("Charizard", 20);
console.log(charizard);

let squirtle = new createPokemon("Squirtle", 30);
console.log(squirtle);



squirtle.tackle(pikachu);
